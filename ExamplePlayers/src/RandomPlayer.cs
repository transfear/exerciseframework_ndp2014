﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NDP2014_Simulation.Data;
using FrameworkInterface.Player;

namespace ExamplePlayers
{
	public class RandomPlayer : IPlayer
	{
		Random mRNG = new Random();

		public RandomPlayer() { }

		public override string GetPlayerName()
		{
			return "Random player";
		}

		public override IPlayerOutput OnGameStart(IPlayerInput _input)
		{
			return null;
		}

		public override IPlayerOutput Update(IPlayerInput _input)
		{
			PlayerOutput output = new PlayerOutput();

			output.fThrottle = (float)(Math.Pow(mRNG.NextDouble(), 0.25));
			output.fBrake    = 1.0f - output.fThrottle;
            output.fSteering = (float)mRNG.NextDouble() * 2.0f - 1.0f;

			return output;
		}

		public override IPlayerOutput OnGameEnd(IPlayerInput _input)
		{
			return null;
		}
	}
}
