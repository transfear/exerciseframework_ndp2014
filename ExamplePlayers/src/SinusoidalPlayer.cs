﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NDP2014_Simulation.Data;
using FrameworkInterface.Player;

namespace ExamplePlayers
{
	public class SinusoidalPlayer : IPlayer
	{
		float mfIncrement = 0.0f;


		public SinusoidalPlayer() { }

		public override string GetPlayerName()
		{
			return "Sinusoidal player";
		}

		public override IPlayerOutput OnGameStart(IPlayerInput _input)
		{
			mfIncrement = 0.0f;
			return null;
		}

		public override IPlayerOutput Update(IPlayerInput _input)
		{
			PlayerInput simInput = _input as PlayerInput;
			if (simInput == null)
				return null;

			PlayerInput.PlayerData myData = simInput.playerData;
			
			// Am I going backward?
			bool bBackward = myData.vDirection.DotProduct(myData.vVelocity) < -0.1f;

			PlayerOutput toReturn = new PlayerOutput();
			toReturn.fSteering = (float)Math.Sin(mfIncrement) * 0.05f;

			if (bBackward)
			{
				toReturn.fThrottle = 0.0f;
				toReturn.fBrake    = 1.0f;
			}
			else
			{
				toReturn.fThrottle = 1.0f;
				toReturn.fBrake    = 0.0f;
			}

			mfIncrement += 0.05f;
			return toReturn;
		}

		public override IPlayerOutput OnGameEnd(IPlayerInput _input)
		{
			return null;
		}
	}
}
