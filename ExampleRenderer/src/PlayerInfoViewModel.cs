﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace NDP2014_Renderer.src
{
	internal class PlayerInfoViewModel : INotifyPropertyChanged
	{
		private String      mPlayerName      = "";
		private int         miPlayerDistance = -1;
		private int         miPlayerHeading  =  0;
		private ImageSource mArrow           = null;

		public String PlayerName
		{
			get { return mPlayerName; }
			set
			{
				mPlayerName = value;
				NotifyPropertyChanged("PlayerName");
			}
		}

		public int PlayerDistance
		{
			get { return miPlayerDistance; }
			set
			{
				miPlayerDistance = value;
				NotifyPropertyChanged("PlayerDistance");
			}
		}

		public int PlayerHeading
		{
			get { return miPlayerHeading; }
			set
			{
				miPlayerHeading = value;
				NotifyPropertyChanged("PlayerHeading");
			}
		}

		public ImageSource PlayerArrow
		{
			get { return mArrow; }
			set
			{
				mArrow = value;
				NotifyPropertyChanged("PlayerArrow");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void NotifyPropertyChanged(String _szPropertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(_szPropertyName));
		}
	}
}
