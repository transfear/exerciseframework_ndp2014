﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using NDP2014_Simulation.Data;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace NDP2014_Renderer
{
	public class Renderer : IRenderer
	{
		UI.MainWindow mWindow   = null;
		Thread        mUIThread = null;

		public IRendererFeedback OnGameStart(ISimulationOutput _simulationData)
		{
			SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput == null)
				return null;

			mUIThread = new Thread(() =>
				{
					mWindow = new UI.MainWindow();
					mWindow.OnGameStart(simOutput);

					Application testApp = new Application();
					testApp.Run(mWindow);
					
				}
			);
			mUIThread.SetApartmentState(ApartmentState.STA);
			mUIThread.Start();

			return null;
		}

		public IRendererFeedback Update(ISimulationOutput _simulationData)
		{
			SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput == null)
				return null;

			// Create feedback for the simulation
			RendererFeedback feedback = new RendererFeedback();
			feedback.ShouldQuit = false;

			if (mWindow != null)
			{
				mWindow.Dispatcher.BeginInvoke((Action)(() => { mWindow.UpdateUI(simOutput); }));				
				feedback.ShouldQuit = mWindow.CloseRequested;
			}

			return feedback;
		}

		public IRendererFeedback OnGameEnd(ISimulationOutput _simulationData)
		{
			SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput != null)
			{
				// NDP 2014 TODO : Message box with best player?
			}

			// Close the window
			mWindow.Dispatcher.Invoke((Action)(() => { mWindow.Close(); }));
			mWindow = null;

			// Wait for the thread to finish
			mUIThread.Join();

			return null;
		}
	}
}
