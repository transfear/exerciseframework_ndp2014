﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using FrameworkInterface.Player;
using NDP2014_Simulation.Data;
using NDP2014_Renderer.src;

namespace NDP2014_Renderer.UI
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private int                                      miGridWidth;
		private int                                      miGridHeigth;
		private BitmapSource                             mArrowBitmap      = null;
		private byte[]                                   mArrowPixels      = null;
		private byte[]                                   mBackGround       = null;
		private Dictionary<IPlayer, PlayerInfoViewModel> mPlayerScoreboard = new Dictionary<IPlayer, PlayerInfoViewModel>();

		public bool CloseRequested { get; set; }

		public MainWindow()
		{
			InitializeComponent();

			CloseRequested = false;
			Closing += OnWindowClosing;

			mArrowBitmap = Bitmap_To_BitmapSource(new Bitmap(typeof(MainWindow).Assembly.GetManifestResourceStream("NDP2014_Renderer.rc.arrow.png")));

			int iW   = mArrowBitmap.PixelWidth;
			int iH   = mArrowBitmap.PixelHeight;
			int iBPP = 4;

			int    iNumPixels = iW * iH;
			int    iBufSize   = iNumPixels * iBPP;
			mArrowPixels      = new byte[iBufSize];
			mArrowBitmap.CopyPixels(mArrowPixels, iW * iBPP, 0);
		}

		[System.Runtime.InteropServices.DllImport("gdi32.dll")]
		public static extern bool DeleteObject(IntPtr hObject);

		private BitmapSource Bitmap_To_BitmapSource(Bitmap _bm)
		{
			IntPtr hBitmap = _bm.GetHbitmap();
			BitmapSource retval;

			try
			{
				retval = Imaging.CreateBitmapSourceFromHBitmap(
							 hBitmap,
							 IntPtr.Zero,
							 Int32Rect.Empty,
							 BitmapSizeOptions.FromEmptyOptions());
			}
			finally
			{
				DeleteObject(hBitmap);
			}

			return retval;
		}

		public void OnGameStart(SimulationOutput _simOutput)
		{
			miGridWidth  = (int)_simOutput.uiGridWidth;
			miGridHeigth = (int)_simOutput.uiGridHeight;

			InitScoreBoard(_simOutput);
			InitBackGround(_simOutput);
		}

		private void InitScoreBoard(SimulationOutput _simOutput)
		{
			mPlayerScoreboard.Clear();

			grdPlayers.RowDefinitions.Clear();
			grdPlayers.Children.Clear();

			int iNumColumns = grdPlayers.ColumnDefinitions.Count();
			int iNumPlayers = _simOutput.players.Count();
			int iNumRows = (iNumPlayers + iNumColumns - 1) / iNumColumns;

			int iCurPlayer = 0;
			for (int iCurRow = 0; iCurRow < iNumRows; ++iCurRow)
			{
				grdPlayers.RowDefinitions.Add(new RowDefinition());

				for (int iCurCol = 0; iCurCol < iNumColumns; ++iCurCol)
				{
					// Skip if all players have been added
					if (iCurPlayer >= iNumPlayers)
						break;

					SimulationOutput.PlayerData curPlayer = _simOutput.players[iCurPlayer];

					PlayerInfo playerInfo = new PlayerInfo();
					PlayerInfoViewModel playerVM = CreateViewModel(curPlayer, playerInfo);
					playerInfo.DataContext = playerVM;

					Grid.SetRow(playerInfo, iCurRow);
					Grid.SetColumn(playerInfo, iCurCol);

					grdPlayers.Children.Add(playerInfo);

					mPlayerScoreboard.Add(curPlayer.player, playerVM);
					++iCurPlayer;
				}
			}
		}

		private PlayerInfoViewModel CreateViewModel(SimulationOutput.PlayerData _simPlayer, PlayerInfo _userCtrl)
		{
			PlayerInfoViewModel playerVM = new PlayerInfoViewModel();

			playerVM.PlayerName     = _simPlayer.player.GetPlayerName();
			playerVM.PlayerHeading  = (int)(_simPlayer.fHeading / (-2.0f * Math.PI) * 360.0f);
			playerVM.PlayerDistance = miGridWidth - (int)_simPlayer.curPos.fX;

			// Create an arrow bitmap of the same color as the player
			byte uiR = _simPlayer.color.R;
			byte uiG = _simPlayer.color.G;
			byte uiB = _simPlayer.color.B;

			int iW   = mArrowBitmap.PixelWidth;
			int iH   = mArrowBitmap.PixelHeight;
			int iBPP = 4;

			int iNumPixels = iW * iH;
			int iBufSize   = iNumPixels * iBPP;
			byte[] srcPixels = new byte[iBufSize];
			mArrowBitmap.CopyPixels(srcPixels, iW * iBPP, 0);

			int iCurByte = 0;
			for (int iY = 0; iY < iH; ++iY)
			{
				for (int iX = 0; iX < iW; ++iX)
				{
					srcPixels[iCurByte + 0] = uiB;
					srcPixels[iCurByte + 1] = uiG;
					srcPixels[iCurByte + 2] = uiR;
					iCurByte += 4;
				}
			}

			WriteableBitmap wr = new WriteableBitmap(iW, iH, 96, 96, PixelFormats.Bgra32, null);
			wr.WritePixels(new Int32Rect(0, 0, iW, iH), srcPixels, iW * iBPP, 0);
			playerVM.PlayerArrow = wr;

			return playerVM;
		}


		private void InitBackGround(SimulationOutput _simOutput)
		{
			int iW   = miGridWidth;
			int iH   = miGridHeigth;
			int iBPP = 3;	// RGB24

			int iNumPixels = iW * iH;
			int iBufSize   = iNumPixels * iBPP;

			mBackGround = new byte[iBufSize];
			
			// Init all black
			for (int iCurByte = 0; iCurByte < iBufSize; ++iCurByte)
				mBackGround[iCurByte] = 100;
			

			// Draw obstacles in grey
			foreach (Obstacle curObstacle in _simOutput.obstacles)
			{
				int iObstacleX = (int)curObstacle.uiX;
				int iObstacleY = (int)curObstacle.uiY;
				int iObstacleW = (int)curObstacle.uiWidth;
				int iObstacleH = (int)curObstacle.uiHeight;
				
				int iStartRow = iObstacleY;
				int iEndRow   = Math.Min(iStartRow + iObstacleH, miGridHeigth);

				int iStartCol = iObstacleX;
				int iEndCol   = Math.Min(iStartCol + iObstacleW, miGridWidth);

				// Draw one row at a time
				for (int iCurRow = iStartRow; iCurRow < iEndRow; ++iCurRow)
				{
					int iRowStart = iCurRow * iW * iBPP ;	// in bytes
					
					// Loop on all columns of this row
					for (int iCurCol = iStartCol; iCurCol < iEndCol; ++iCurCol)
					{
						int iByteOffset = iRowStart + iCurCol * iBPP;
						mBackGround[iByteOffset + 0] = 196;
						mBackGround[iByteOffset + 1] = 196;
						mBackGround[iByteOffset + 2] = 196;
					}
				}
			}
		}

		public void UpdateUI(SimulationOutput _simOutput)
		{
			int iW   = miGridWidth;
			int iH   = miGridHeigth;
			int iBPP = 3;
			
			int    iBufSize  = mBackGround.GetLength(0);
			byte[] pixelData = new byte[iBufSize];
			Array.Copy(mBackGround, pixelData, iBufSize);

			// Generate players
			foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
			{
				System.Drawing.Color playerColor = curPlayer.color;
				Int32 iPosPixelIndex = (Int32)((curPlayer.curPos.fX + curPlayer.curPos.fY * iW) * iBPP);

				DrawPlayer(curPlayer, pixelData);

				// Also update players' scoreboard
				UpdatePlayerScoreBoard(curPlayer);
			}

			// Update image
			WriteableBitmap wb = new WriteableBitmap(iW, iH, 96, 96, PixelFormats.Rgb24, null);
			wb.WritePixels(new Int32Rect(0, 0, iW, iH), pixelData, iW * iBPP, 0);
			imgSimulation.Source = wb;
		}

		private void DrawPlayer(SimulationOutput.PlayerData _simPlayer, byte[] _dstPixels)
		{
			int iBPP     = 3;
			int iPlayerX = (int)_simPlayer.curPos.fX;
			int iPlayerY = (int)_simPlayer.curPos.fY;

			float fAngle = _simPlayer.fHeading;
			float fSin   = (float)Math.Sin(fAngle);
			float fCos   = (float)Math.Cos(fAngle);

			// Generate a rotated arrow, only using alpha channel of source arrow image
			int iSrcW = mArrowBitmap.PixelWidth;
			int iSrcH = mArrowBitmap.PixelHeight;

			float fHalfSrcW = (float)Math.Ceiling((float)iSrcW * 0.5f);
			float fHalfSrcH = (float)Math.Ceiling((float)iSrcH * 0.5f);

			int   iSqrDim  = (int)Math.Ceiling(Math.Sqrt(iSrcW * iSrcW + iSrcH * iSrcH));
			float fHalfDim = (float)iSqrDim * 0.5f;
			int   iHalfDim = (int)(fHalfDim + 0.5f);

			int iDstBaseX = iPlayerX - iHalfDim;
			int iDstBaseY = iPlayerY - iHalfDim;

			for (int iY = 0; iY < iSqrDim; ++iY)
			{
				int iDstY = iDstBaseY + iY;

				// Skip if near top portion
				if (iDstY < 0)
					continue;

				// Skip if near bot portion
				if (iDstY >= miGridHeigth)
					continue;

				float fY    = (float)Math.Floor((float)iY - fHalfDim);
				float fYSin = fY * fSin;
				float fYCos = fY * fCos;

				for (int iX = 0; iX < iSqrDim; ++iX)
				{
					int iDstX = iDstBaseX + iX;

					// Skip if near left portion
					if (iDstX < 0)
						continue;

					// Skip if near right portion
					if (iDstX >= miGridWidth)
						continue;

					float fX    = (float)Math.Floor((float)iX - fHalfDim);
					float fXSin = fX * fSin;
					float fXCos = fX * fCos;

					float fRotatedX = fXCos - fYSin;
					float fRotatedY = fXSin + fYCos;

					fRotatedX += fHalfSrcW;
					fRotatedY += fHalfSrcH;

					float fAlpha = BilinearFetch_Alpha(mArrowPixels, iSrcW, iSrcH, fRotatedX, fRotatedY);

					// Skip if completely transparent
					if (fAlpha == 0.0f)
						continue;

					// Blend
					int iDstOffset = (iDstY * miGridWidth + iDstX) * iBPP;
					BlendRGB(_simPlayer.color, fAlpha, _dstPixels, iDstOffset);
				}
			}
		}

		private void BlendRGB(System.Drawing.Color _src, float _srcAlpha, byte[] _dstBuf, int _iDstOffset)
		{
			float fInvAlpha = 1.0f - _srcAlpha;

			float fDstR = (float)_dstBuf[_iDstOffset + 0] * fInvAlpha;
			float fDstG = (float)_dstBuf[_iDstOffset + 1] * fInvAlpha;
			float fDstB = (float)_dstBuf[_iDstOffset + 2] * fInvAlpha;

			float fSrcR = (float)_src.R * _srcAlpha;
			float fSrcG = (float)_src.G * _srcAlpha;
			float fSrcB = (float)_src.B * _srcAlpha;

			_dstBuf[_iDstOffset + 0] = (byte)(fSrcR + fDstR);
			_dstBuf[_iDstOffset + 1] = (byte)(fSrcG + fDstG);
			_dstBuf[_iDstOffset + 2] = (byte)(fSrcB + fDstB);
		}

		private float BilinearFetch_Alpha(byte[] _srcData, int _iW, int _iH, float _fX, float _fY)
		{
			int iBPP = 4;

			int iX0 = (int)Math.Floor(_fX);
			int iY0 = (int)Math.Floor(_fY);
			int iX1 = iX0 + 1;
			int iY1 = iY0 + 1;

			float fRightWeight = (iX1 >= 0 && iX1 < _iW) ? _fX - (float)iX0    : 0.0f;
			float fLeftWeight  = (iX0 >= 0 && iX0 < _iW) ? 1.0f - fRightWeight : 0.0f;
			float fBotWeight   = (iY1 >= 0 && iY1 < _iH) ? _fY - (float)iY0    : 0.0f;
			float fTopWeight   = (iY0 >= 0 && iY0 < _iH) ? 1.0f - fBotWeight   : 0.0f;

			float fTopLeftW  = fTopWeight * fLeftWeight;
			float fTopRightW = fTopWeight * fRightWeight;
			float fBotLeftW  = fBotWeight * fLeftWeight;
			float fBotRightW = fBotWeight * fRightWeight;

			float fTopLeft  = (fTopLeftW  > 0.0f) ? (float)_srcData[(iY0 * _iW + iX0) * iBPP + 3] * fTopLeftW  : 0.0f;
			float fTopRight = (fTopRightW > 0.0f) ? (float)_srcData[(iY0 * _iW + iX1) * iBPP + 3] * fTopRightW : 0.0f;
			float fBotLeft  = (fBotLeftW  > 0.0f) ? (float)_srcData[(iY1 * _iW + iX0) * iBPP + 3] * fBotLeftW  : 0.0f;
			float fBotRight = (fBotRightW > 0.0f) ? (float)_srcData[(iY1 * _iW + iX1) * iBPP + 3] * fBotRightW : 0.0f;

			float fTotalValue = fTopLeft + fTopRight + fBotLeft + fBotRight;
			return fTotalValue / 255.0f;
		}

		private void UpdatePlayerScoreBoard(SimulationOutput.PlayerData _simPlayer)
		{
			PlayerInfoViewModel playerVM = mPlayerScoreboard[_simPlayer.player];
			playerVM.PlayerHeading  = (int)(_simPlayer.fHeading / (-2.0f * Math.PI) * 360.0f);
			playerVM.PlayerDistance = miGridWidth - (int)_simPlayer.curPos.fX;
		}

		private void OnWindowClosing(object sender, CancelEventArgs e)
		{
			CloseRequested = true;
		}
	}
}
