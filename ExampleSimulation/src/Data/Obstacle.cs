﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NDP2014_Simulation.Data
{
	public struct Obstacle
	{
		public UInt32 uiX;
		public UInt32 uiY;
		public UInt32 uiWidth;
		public UInt32 uiHeight;

		public bool IsPointInside(Vector2d _vPos)
		{
			if (_vPos.fX < uiX)
				return false;

			if (_vPos.fX >= uiX + uiWidth)
				return false;

			if (_vPos.fY < uiY)
				return false;

			if (_vPos.fY >= uiY + uiHeight)
				return false;

			return true;
		}
	}
}
