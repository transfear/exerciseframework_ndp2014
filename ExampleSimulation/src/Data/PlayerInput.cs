﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace NDP2014_Simulation.Data
{
	public class PlayerInput : IPlayerInput
	{
		public class PlayerData
		{
			public UInt32 uiPlayerIndex;

			public Vector2d vPosition  = new Vector2d();
			public Vector2d vDirection = new Vector2d();
			public Vector2d vVelocity  = new Vector2d();

			public PlayerData CopyDeep()
			{
				PlayerData copy = new PlayerData();
				copy.uiPlayerIndex = uiPlayerIndex;
				copy.vPosition     = vPosition;
				copy.vDirection    = vDirection;
				copy.vVelocity     = vVelocity;

				return copy;
			}
		};

		public TimeSpan    deltaT;
		public PlayerData  playerData;
		public Obstacle[]  obstacles;
		public UInt32      uiGridWidth;
		public UInt32      uiGridHeight;

		// From IPlayerInputInterface
		public IPlayerInput CopyDeep()
		{
			PlayerInput copy = new PlayerInput();

			// Copy player data
			copy.playerData = playerData.CopyDeep();

			// Copy obstacles
			Int32 iNumObstacles = obstacles.GetLength(0);
			copy.obstacles = new Obstacle[iNumObstacles];
			Array.Copy(obstacles, copy.obstacles, iNumObstacles);

			copy.deltaT       = deltaT;
			copy.uiGridWidth  = uiGridWidth;
			copy.uiGridHeight = uiGridHeight;

			return copy;
		}
	}
}
