﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace NDP2014_Simulation.Data
{
	public class PlayerOutput : IPlayerOutput
	{
		public float fThrottle;		// Between  0.0f and 1.0f.  0.0f == no throttle, 1.0f == full throttle
		public float fBrake;		// Between  0.0f and 1.0f.  0.0f == no brake, 1.0f == full brakes
		public float fSteering;		// Between -1.0f and 1.0f. -1.0f == full left, 1.0f == full right. Represent wheel angle in radia

		// From IPlayerOutput interface
		public IPlayerOutput CopyDeep()
		{
			PlayerOutput copy = new PlayerOutput();
			copy.fThrottle = fThrottle;
			copy.fBrake    = fBrake;
			copy.fSteering = fSteering;

			return copy;
		}
	}
}
