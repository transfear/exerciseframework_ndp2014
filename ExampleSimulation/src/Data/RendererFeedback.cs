﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Renderer;

namespace NDP2014_Simulation.Data
{
	public class RendererFeedback : IRendererFeedback
	{
		public bool ShouldQuit { get; set; }

		// From IRendererFeedback interface
		public IRendererFeedback CopyDeep()
		{
			RendererFeedback copy = new RendererFeedback();
			copy.ShouldQuit = ShouldQuit;
			return copy;
		}
	}
}
