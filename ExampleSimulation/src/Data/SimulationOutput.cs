﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using FrameworkInterface.Simulation;

namespace NDP2014_Simulation.Data
{
	public class SimulationOutput : ISimulationOutput
	{
		public class PlayerData
		{
			public FrameworkInterface.Player.IPlayer player = null;

			public Color        color;
			public Vector2d     curPos   = new Vector2d();
			public float        fHeading = 0.0f;			// Between 0 and 2PI. 0 == right, PI/2 == up, PI == left, 3PI/2 == bottom

			public PlayerData CopyDeep()
			{
				PlayerData copy = new PlayerData();
				copy.player   = player;
				copy.color    = color;
				copy.curPos   = curPos;
				copy.fHeading = fHeading;

				return copy;
			}
			
		};

		public PlayerData[] players;
		public Obstacle[]   obstacles;

		public UInt32       uiCurrentTurn = 0;
		public UInt32       uiGridWidth   = 0;
		public UInt32       uiGridHeight  = 0;
		

		// From ISimulationOutput interface
		public ISimulationOutput CopyDeep()
		{
			SimulationOutput copy = new SimulationOutput();

			// Copy players
			Int32 iNumPlayers = players.GetLength(0);
			copy.players = new PlayerData[iNumPlayers];
			for (Int32 iCurPlayer = 0; iCurPlayer < iNumPlayers; ++iCurPlayer)
				copy.players[iCurPlayer] = players[iCurPlayer].CopyDeep();

			// Copy obstacles
			Int32 iNumObstacles = obstacles.GetLength(0);
			copy.obstacles = new Obstacle[iNumObstacles];
			Array.Copy(obstacles, copy.obstacles, iNumObstacles);

			copy.uiCurrentTurn = uiCurrentTurn;
			copy.uiGridWidth   = uiGridWidth;
			copy.uiGridHeight  = uiGridHeight;

			return copy;
		}
	}
}
