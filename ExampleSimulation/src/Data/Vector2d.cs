﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NDP2014_Simulation.Data
{
	public struct Vector2d
	{
		public float fX;
		public float fY;

		public void Add(Vector2d _vAdd)
		{
			fX += _vAdd.fX;
			fY += _vAdd.fY;
		}

		public void Subtract(Vector2d _vSub)
		{
			fX -= _vSub.fX;
			fY -= _vSub.fY;
		}

		public void Multiply(float _fMultiplier)
		{
			fX *= _fMultiplier;
			fY *= _fMultiplier;
		}

		public void Divide(float _fDivider)
		{
			fX /= _fDivider;
			fY /= _fDivider;
		}

		public float SqrLength()
		{
			return fX * fX + fY * fY;
		}

		public float Length()
		{
			return (float)Math.Sqrt(fX * fX + fY * fY);
		}

		public float SqrDistance(Vector2d _vTo)
		{
			_vTo.Subtract(this);
			return _vTo.SqrLength();
		}

		public float Distance(Vector2d _vTo)
		{
			_vTo.Subtract(this);
			return _vTo.Length();
		}

		public float DotProduct(Vector2d _vOther)
		{
			return fX * _vOther.fX + fY * _vOther.fY;
		}

		public void Rotate(float _fAngleRad)
		{
			float fNewfX = (float)(fX * Math.Cos(_fAngleRad) - fY * Math.Sin(_fAngleRad));
			float fNewfY = (float)(fX * Math.Sin(_fAngleRad) + fY * Math.Cos(_fAngleRad));

			fX = fNewfX;
			fY = fNewfY;
		}
	}
}
