﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NDP2014_Simulation.Data;

using FrameworkInterface.Player;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace NDP2014_Simulation.Internal
{
	class BaseSimulation
	{
		#region Public Interface
		public ICollection<IPlayer> PlayerList    { get; set; }
		public IPlayer              CurrentPlayer { get; set; }

		public void OnGameStart(ICollection<IPlayer> _playerCollection)
		{
			PlayerList = _playerCollection;
			Init();
		}

		public IPlayerInput ProducePlayerInput(IPlayer _player)
		{
			CarData curPlayer = mCarDataMap[_player];
			
			PlayerInput.PlayerData playerData = new PlayerInput.PlayerData();
			playerData.uiPlayerIndex = _player.PlayerIndex;
			playerData.vPosition     = curPlayer.carPosition;
			playerData.vDirection    = curPlayer.carDirection;
			playerData.vVelocity     = curPlayer.carVelocity;

			PlayerInput toReturn  = new PlayerInput();
			toReturn.playerData   = playerData;
			toReturn.obstacles    = mObstacles;
			toReturn.uiGridWidth  = (UInt32)miGridWidth;
			toReturn.uiGridHeight = (UInt32)miGridHeight;
			toReturn.deltaT       = curPlayer.lastDeltaT;

			return toReturn;
		}

		public void ApplyPlayerOuput(IPlayer _player, IPlayerOutput _playerResponse)
		{
			PlayerOutput response = _playerResponse as PlayerOutput;
			if (response == null)
				return;

			// We just received a response from a player, reset its delta T
			CarData playerData    = mCarDataMap[_player];
			playerData.lastDeltaT = playerData.timer.Elapsed;
			playerData.timer.Restart();

			// Validate response
			response.fThrottle = Math.Min(Math.Max(response.fThrottle,  0.0f), 1.0f);
			response.fBrake    = Math.Min(Math.Max(response.fBrake,     0.0f), 1.0f);
			response.fSteering = Math.Min(Math.Max(response.fSteering, -1.0f), 1.0f);

			// Store for next update
			mLastOutputMap[_player] = response;
		}

		public ISimulationOutput ProduceSimulationOutput()
		{
			UpdateTurn();

			SimulationOutput toReturn = new SimulationOutput();
			toReturn.players        = mPlayerMap.Values.ToArray();
			toReturn.obstacles      = mObstacles;
			toReturn.uiCurrentTurn  = (UInt32)miCurrentTurn;
			toReturn.uiGridWidth    = (UInt32)miGridWidth;
			toReturn.uiGridHeight   = (UInt32)miGridHeight;

			return toReturn;
		}

		public void ApplyRendererFeedback(IRendererFeedback _rendererFeedback)
		{
			RendererFeedback feedback = _rendererFeedback as RendererFeedback;
			if (feedback == null)
				return;

			// Just check if the renderer wants us to quit
			if (feedback.ShouldQuit)
				mbGameFinished = true;
		}

		public void OnGameEnd()
		{
			// Look for player with the highest X value (closest to finish line)
			if (mPlayerMap.Keys.Count == 0)
				return;

			float fMaxX = -1.0f;
			KeyValuePair<IPlayer, CarData> winner = new KeyValuePair<IPlayer, CarData>();
			foreach (KeyValuePair<IPlayer, CarData> curPlayer in mCarDataMap)
			{
				if (curPlayer.Value.carPosition.fX > fMaxX)
				{
					winner = curPlayer;
					fMaxX = curPlayer.Value.carPosition.fX;
				}
			}

			// Message box with best player
			MessageBox.Show(winner.Key.GetPlayerName() + " won! (X=" + fMaxX.ToString() + ")", "Game finished!");
		}

		public bool IsGameFinished()
		{
			return mbGameFinished;
		}
		#endregion


		#region Private Interface

		Dictionary<IPlayer, PlayerOutput>                mLastOutputMap = new Dictionary<IPlayer, PlayerOutput>();
		Dictionary<IPlayer, SimulationOutput.PlayerData> mPlayerMap     = new Dictionary<IPlayer, SimulationOutput.PlayerData>();
		Dictionary<IPlayer, CarData>                     mCarDataMap    = new Dictionary<IPlayer, CarData>();
		bool mbGameFinished = false;
		
		Int32 miCurrentTurn          = 0;

		Int32 miGridWidth   = 1000;
		Int32 miGridHeight  = 250;

		Random mRNG = new Random();
		
		Obstacle[] mObstacles = null;
		
		void Init()
		{
            string strCurDirectory = Directory.GetCurrentDirectory();
            bool   bSamePos        = File.Exists(Path.Combine(strCurDirectory, "SamePos.txt"));
            int    iSamePosY       = mRNG.Next(0, miGridHeight);

			miCurrentTurn  = 0;
			mbGameFinished = false;

			int   iNumPlayers   = PlayerList.Count();
			float fHueIncrement = (iNumPlayers > 0) ? 1.0f / (float)iNumPlayers : 1.0f;
			float fRandomHue    = (float)mRNG.NextDouble();

			// Fill player map
			mPlayerMap.Clear();
			mCarDataMap.Clear();
			mLastOutputMap.Clear();
			foreach (IPlayer curPlayer in PlayerList)
			{
				SimulationOutput.PlayerData newPlayerData = new SimulationOutput.PlayerData();
				CarData                     newCarData    = new CarData();

				newPlayerData.player = curPlayer;
				newPlayerData.color = GetColorFromHue(fRandomHue);
				fRandomHue          = (fRandomHue + fHueIncrement) % 1.0f;

				newPlayerData.curPos.fX = 0;
				newPlayerData.curPos.fY = (bSamePos) ? iSamePosY : mRNG.Next(0, miGridHeight);
				newPlayerData.fHeading = 0.0f;

				newCarData.carPosition = newPlayerData.curPos;
				newCarData.oldPosition = newCarData.carPosition;

				mPlayerMap.Add(curPlayer, newPlayerData);
				mCarDataMap.Add(curPlayer, newCarData);
				mLastOutputMap.Add(curPlayer, new PlayerOutput());
			}

			// Generate obstacles
			ObstacleGenerator generator = new ObstacleGenerator(mRNG, miGridWidth, miGridHeight);
			mObstacles = generator.GetRandomObstacles();
		}

		Color GetColorFromHue(float _fHue)
		{
			float fR = 0.0f;
			float fG = 0.0f;
			float fB = 0.0f;

			const float fOneThird = 1.0f / 3.0f;
			const float fTwoThirds = 2.0f / 3.0f;

			if (_fHue < fOneThird)
			{
				float fLerp = _fHue / fOneThird;
				if (fLerp < 0.5f)
				{
					fLerp *= 2.0f;
					fR     = 1.0f;
					fG     = Math.Max(fG, fLerp);
				}
				else
				{
					fLerp = (fLerp - 0.5f) * 2.0f;
					fG    = 1.0f;
					fR    = Math.Max(fR, 1.0f - fLerp);
				}

			}
			else if (_fHue < fTwoThirds)
			{
				_fHue -= fOneThird;
				float fLerp = _fHue / fOneThird;
				if (fLerp < 0.5f)
				{
					fLerp *= 2.0f;
					fG     = 1.0f;
					fB     = Math.Max(fB, fLerp);
				}
				else
				{
					fLerp = (fLerp - 0.5f) * 2.0f;
					fB    = 1.0f;
					fG    = Math.Max(fG, 1.0f - fLerp);
				}
			}
			else
			{
				_fHue -= fTwoThirds;
				float fLerp = _fHue / fOneThird;
				if (fLerp < 0.5f)
				{
					fLerp *= 2.0f;
					fB     = 1.0f;
					fR     = Math.Max(fR, fLerp);
				}
				else
				{
					fLerp = (fLerp - 0.5f) * 2.0f;
					fR    = 1.0f;
					fB    = Math.Max(fB, 1.0f - fLerp);
				}
			}


			return Color.FromArgb(255, (byte)(fR * 255.0f), (byte)(fG * 255.0f), (byte)(fB * 255.0f));
		}

		void UpdateTurn()
		{

			// Update player and car data
			foreach (SimulationOutput.PlayerData curPlayerData in mPlayerMap.Values)
			{
				PlayerOutput lastOutput = mLastOutputMap[curPlayerData.player];
				CarData carData = mCarDataMap[curPlayerData.player];

				carData.carDirection.fX = (float)Math.Cos(curPlayerData.fHeading);
				carData.carDirection.fY = -(float)Math.Sin(curPlayerData.fHeading);

				//straight line car simulation computation
				Vector2d forceTraction = carData.carDirection;

				//check for obstacle
				bool hitSomething   = false;
				int  iObstacleIndex = 0;
				foreach (Obstacle curObstacle in mObstacles)
				{
					if (curObstacle.IsPointInside(curPlayerData.curPos))
					{
						//we have it something                       
						hitSomething = true;
						break;
					}

					++iObstacleIndex;
				}

				// Apply throttle
				forceTraction.Multiply(CarData.fEngineForce * lastOutput.fThrottle);

				//air resistance
				Vector2d forceDrag = carData.carVelocity;

				//magic number, drag air resistant constant for a Corvette on earth
				const float fAirResistance = -0.4257f;

				//forceDrag.Length == speed
				forceDrag.Multiply(forceDrag.Length());
				forceDrag.Multiply(fAirResistance);
				//rolling resistance
				Vector2d forceRollResistance = carData.carVelocity;

				//Rolling resistance constant is 30 time higther then air resistancec constant
				forceRollResistance.Multiply(fAirResistance * 30);

				Vector2d forceLongtitudinal = new Vector2d();
				forceLongtitudinal.Add(forceTraction);
				forceLongtitudinal.Add(forceDrag);
				forceLongtitudinal.Add(forceRollResistance);

				//divide by mass give us acceleration
				forceLongtitudinal.Divide(CarData.fCarMass);

				float fDeltaT = 1.0f / 30.0f;
				forceLongtitudinal.Multiply(fDeltaT);

				//adjust velocity for brake and collision
				bool goingBackward = false;
				if (lastOutput.fBrake > lastOutput.fThrottle)
				{
					if (forceLongtitudinal.Length() > carData.carVelocity.Length())
					{
						//no reverse when braking ! Clamp to 0
						//remove this to allow going backward using brake
						forceLongtitudinal.fX = carData.carVelocity.fX * -1.0f;
						forceLongtitudinal.fY = carData.carVelocity.fY * -1.0f;
					}
				}

				carData.carVelocity.Add(forceLongtitudinal);

				// Apply brakes
				float fBrakes = Math.Max(1.0f - lastOutput.fBrake * CarData.fBrakeScale * fDeltaT, 0.0f);
				carData.carVelocity.Multiply(fBrakes);

                // Clamp position on edges
                if (carData.carPosition.fX < 0.0f || carData.carPosition.fX > miGridWidth
                    || carData.carPosition.fY < 0.0f || carData.carPosition.fY > miGridHeight)
                    hitSomething = true;

				if (hitSomething)
				{
					// Teleport player out of obstacle
					carData.carPosition = carData.oldPosition;

					//hitting something mean receiving a greater opposite force (in this game at least)
					//this will force the player backward
					float fSpeed = carData.carVelocity.Length();
					if (fSpeed > 0.0f)
					{
						carData.carVelocity.Divide(fSpeed);
						carData.carVelocity.Multiply(-27.0f);
					}
				}

				//handle reverse
				if (Math.Sign(carData.carDirection.fX) != Math.Sign(carData.carVelocity.fX) ||
						Math.Sign(carData.carDirection.fY) != Math.Sign(carData.carVelocity.fY))
					goingBackward = true;


				Vector2d velocityOnPlayer = carData.carVelocity;
				velocityOnPlayer.Multiply(fDeltaT);
				//player pos += velocityOnPlayer

				//now calculate steering for this player
				//every good side project need a complicated use of ternary operator (clamp value)
				float fSteeringRad = lastOutput.fSteering < -1.0f ? -1.0f : ((lastOutput.fSteering > 1.0f) ? 1.0f : lastOutput.fSteering);
				float fTurningCircleRadius = (float)(CarData.fCarLength / fSteeringRad);

				float fAngularVelocity = carData.carVelocity.Length() / fTurningCircleRadius; // in rad per sec

				carData.carVelocity.Rotate(fAngularVelocity * fDeltaT);

				//velocity in meter/s
				float fVelLen = carData.carVelocity.Length();

				if (fVelLen > 0.0f)
				{
					carData.carDirection = carData.carVelocity;
					carData.carDirection.Divide(fVelLen);

					if (goingBackward)
						carData.carDirection.Multiply(-1.0f);
				}

				// speed * deltaT for displacement
				carData.oldPosition = carData.carPosition;
				carData.carPosition.fX += carData.carVelocity.fX * fDeltaT;
				carData.carPosition.fY += carData.carVelocity.fY * fDeltaT;

				
				

				// Feed infos for renderer
				float fHeading = (float)(-Math.Atan2(carData.carDirection.fX, -carData.carDirection.fY) + Math.PI * 0.5);
				if (fHeading < 0.0f)
					fHeading += (float)Math.PI * 2.0f;
				if (fHeading > (float)Math.PI * 2.0f)
					fHeading -= (float)Math.PI * 2.0f;

				//keep current heading when hitting wall

				curPlayerData.fHeading = fHeading;
				curPlayerData.curPos = carData.carPosition;
			}
			
			++miCurrentTurn;

			// Check if a player has reached the finish line
			foreach (SimulationOutput.PlayerData curData in mPlayerMap.Values)
			{
				if (curData.curPos.fX >= miGridWidth)
				{
					mbGameFinished = true;
					break;
				}
			}
		}
		
		#endregion
	}
}
