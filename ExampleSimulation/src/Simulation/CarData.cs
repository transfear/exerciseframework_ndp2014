﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using NDP2014_Simulation.Data;

namespace NDP2014_Simulation.Internal
{
	class CarData
	{
		public const float fEngineForce = 2300; //rpm - input param
		public const float fBrakeScale  = 0.75f;
		public const float fCarMass     = 125;  //kg
		public const float fCarLength   = 3.0f;

		public Vector2d oldPosition  = new Vector2d();
		public Vector2d carPosition  = new Vector2d();
		public Vector2d carDirection = new Vector2d();
		public Vector2d carVelocity  = new Vector2d();

		public TimeSpan  lastDeltaT = new TimeSpan(0);
		public Stopwatch timer      = new Stopwatch();

		public CarData()
		{
			// Start the Stopwatch as soon as object is created
			timer.Start();
		}
	}
}
