﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NDP2014_Simulation.Data;

namespace NDP2014_Simulation.Internal
{
	class ObstacleGenerator
	{
		class Generator
		{
			public float Probability { get; set; }

			public delegate Obstacle[] GenerateObstaclesFn();
			public GenerateObstaclesFn GenerateObstacles;
		}

		Random          mRNG                 = null;
		List<Generator> mGeneratorList       = new List<Generator>();
		float           mfTotalProbabilities = 0.0f;
		int             miWidth              = 0;
		int             miHeight             = 0;

		public ObstacleGenerator(Random _parentRNG, int _iW, int _iH)
		{
			mRNG     = _parentRNG;
			miWidth  = _iW;
			miHeight = _iH;

			AddGenerator(new Generator { Probability = 0.05f, GenerateObstacles = StaticGeneration_1 });
			AddGenerator(new Generator { Probability = 0.05f, GenerateObstacles = StaticGeneration_2 });
			AddGenerator(new Generator { Probability = 0.50f, GenerateObstacles = RandomGeneration_Uniform });
			AddGenerator(new Generator { Probability = 0.30f, GenerateObstacles = RandomGeneration_Edges_H });
			AddGenerator(new Generator { Probability = 0.25f, GenerateObstacles = RandomGeneration_Edges_V });
			
		}

		private void AddGenerator(Generator _toAdd)
		{
			mGeneratorList.Add(_toAdd);
			mfTotalProbabilities += _toAdd.Probability;
		}

		public Obstacle[] GetRandomObstacles()
		{
			float fDomain = (float)mRNG.NextDouble() * mfTotalProbabilities;

			float     fCurX = 0.0f;
			Generator toUse = null;
			foreach (Generator curGen in mGeneratorList)
			{
				fCurX += curGen.Probability;
				if (fCurX > fDomain)
				{
					toUse = curGen;
					break;
				}
			}

			Obstacle[] toReturn = toUse.GenerateObstacles();
			return toReturn;
		}

		private Obstacle[] StaticGeneration_1()
		{
			Obstacle[] obstacles = new Obstacle[]
			{
				new Obstacle { uiX = 100, uiY =  10, uiWidth =  10, uiHeight =  70 },
				new Obstacle { uiX = 140, uiY = 200, uiWidth =  90, uiHeight =  20 },
				new Obstacle { uiX = 240, uiY =   5, uiWidth = 190, uiHeight =  25 },
				new Obstacle { uiX = 300, uiY =  80, uiWidth =  15, uiHeight =  50 },
				new Obstacle { uiX = 350, uiY = 125, uiWidth =   5, uiHeight =  45 },
				new Obstacle { uiX = 420, uiY = 200, uiWidth =  85, uiHeight =  10 },
				new Obstacle { uiX = 510, uiY =  15, uiWidth =  15, uiHeight =  75 },
				new Obstacle { uiX = 620, uiY =  25, uiWidth =  25, uiHeight = 120 },
				new Obstacle { uiX = 800, uiY = 100, uiWidth =  20, uiHeight = 100 },
			};

			return obstacles;
		}

		private Obstacle[] StaticGeneration_2()
		{
			Obstacle[] obstacles = new Obstacle[]
			{
				new Obstacle { uiX =  50, uiY =  30, uiWidth = 100, uiHeight =  10 },
				new Obstacle { uiX =  50, uiY = 200, uiWidth =  75, uiHeight =   5 },
				new Obstacle { uiX =  50, uiY = 120, uiWidth =  50, uiHeight =  20 },
				new Obstacle { uiX = 190, uiY =  10, uiWidth = 125, uiHeight =  15 },
				new Obstacle { uiX = 170, uiY =  90, uiWidth =  85, uiHeight =  10 },
				new Obstacle { uiX = 210, uiY = 170, uiWidth = 140, uiHeight =  15 },
				new Obstacle { uiX = 290, uiY =  70, uiWidth = 200, uiHeight =  10 },
				new Obstacle { uiX = 290, uiY =  70, uiWidth = 200, uiHeight =  10 },
				new Obstacle { uiX = 350, uiY = 220, uiWidth = 150, uiHeight =  25 },
				new Obstacle { uiX = 500, uiY =  35, uiWidth = 125, uiHeight =  12 },
				new Obstacle { uiX = 520, uiY = 150, uiWidth =  95, uiHeight =   7 },
				new Obstacle { uiX = 590, uiY = 215, uiWidth = 195, uiHeight =   9 },
				new Obstacle { uiX = 630, uiY = 105, uiWidth = 135, uiHeight =  13 },
				new Obstacle { uiX = 750, uiY =  61, uiWidth = 185, uiHeight =  17 },
				new Obstacle { uiX = 799, uiY = 161, uiWidth =  99, uiHeight =  11 },
			};

			return obstacles;
		}

		private Obstacle[] RandomGeneration_Uniform()
		{
            int iInitialBuffer = 50;

			int   iObstacleSize   = mRNG.Next(15) + 5;	// [5, 20]
			int   iObstacleArea   = iObstacleSize * iObstacleSize;
			float fObstacleAmount = (float)mRNG.NextDouble() * 15.0f + 15.0f;

			int iGridSize      = (miWidth - (iObstacleSize * 2)) * (miHeight - (iObstacleSize * 2));
			int iObstacleCount = (int)((float)iGridSize / ((float)iObstacleArea * fObstacleAmount));

			List<Obstacle> list = new List<Obstacle>();
			for (int iCurObstacle = 0; iCurObstacle < iObstacleCount; ++iCurObstacle)
			{
				int iX = mRNG.Next(miWidth  - iObstacleSize - iInitialBuffer) + iInitialBuffer;
				int iY = mRNG.Next(miHeight - iObstacleSize);
				list.Add(new Obstacle { uiX = (uint)iX, uiY = (uint)iY, uiWidth = (uint)iObstacleSize, uiHeight = (uint)iObstacleSize });
			}

			return list.ToArray();
		}

		private Obstacle[] RandomGeneration_Edges_H()
		{
            int iInitialBuffer = 50;

			float fObstacleAmount = (float)mRNG.NextDouble() * (float)miWidth + (float)(miWidth * 1.6f);

			int iGridSize      = miWidth * miHeight;
			int iObstacleCount = (int)((float)iGridSize / fObstacleAmount);

			List<Obstacle> list = new List<Obstacle>();
			for (int iCurObstacle = 0; iCurObstacle < iObstacleCount; ++iCurObstacle)
			{
				int iObstacleWidth  = mRNG.Next(40) + 20;	// [20, 60]
				int iObstacleHeight = iObstacleWidth / 8;

                int iX = mRNG.Next(miWidth - iObstacleWidth * 2 - iInitialBuffer) + iObstacleWidth + iInitialBuffer;

				// Obstacles should more likely appear on edges of the map
				double dY = mRNG.NextDouble() * 2.0 - 1.0;
				dY = Math.Pow(Math.Abs(dY), 0.8) * Math.Sign(dY);

				int iY = (int)((dY * 0.5 + 0.5) * (miHeight - iObstacleHeight));
				list.Add(new Obstacle { uiX = (uint)iX, uiY = (uint)iY, uiWidth = (uint)iObstacleWidth, uiHeight = (uint)iObstacleHeight });
			}

			return list.ToArray();
		}

		private Obstacle[] RandomGeneration_Edges_V()
		{
			float fObstacleAmount = (float)(mRNG.NextDouble() * (float)miWidth + (float)miWidth) * 1.6f + 15.0f;

			int iGridSize = miWidth * miHeight;
			int iObstacleCount = (int)((float)iGridSize / fObstacleAmount);

			int iBufferLeft = miWidth / 10;

			List<Obstacle> list = new List<Obstacle>();
			for (int iCurObstacle = 0; iCurObstacle < iObstacleCount; ++iCurObstacle)
			{
				int iObstacleHeight = mRNG.Next(45) + 20;	// [20, 65]
				int iObstacleWidth = iObstacleHeight / 10;

				int iX = mRNG.Next(miWidth - 2 * iObstacleWidth) + iObstacleWidth + iBufferLeft;

				// Obstacles should more likely appear on edges of the map
				double dY = mRNG.NextDouble() * 2.0 - 1.0;
				dY = Math.Pow(Math.Abs(dY), 0.6) * Math.Sign(dY);

				int iY = (int)((dY * 0.5 + 0.5) * (miHeight - iObstacleHeight));
				list.Add(new Obstacle { uiX = (uint)iX, uiY = (uint)iY, uiWidth = (uint)iObstacleWidth, uiHeight = (uint)iObstacleHeight });
			}

			return list.ToArray();
		}

	}
}
