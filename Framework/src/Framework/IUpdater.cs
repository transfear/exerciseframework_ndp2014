﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Simulation;
using FrameworkInterface.Player;
using FrameworkInterface.Renderer;

namespace Framework
{
	interface IUpdater
	{
		void Update(ISimulation _simulation, IRenderer _renderer, ICollection<IPlayer> _playerCollection);
	}
}
