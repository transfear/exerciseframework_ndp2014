﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using FrameworkInterface.Player;

namespace Framework
{
	class Program
	{
		static FrameworkInterface.Simulation.ISimulation      mSimulation = null;
		static FrameworkInterface.Renderer.IRenderer          mRenderer   = null;
		static ICollection<IPlayer> mPlayers    = null;

		static void Main(string[] args)
		{
			if (Init())
			{
				StartGame();
				RunGame();
				EndGame();
			}
		}

		static bool Init()
		{
			// Look in current directory, and attempt to find DLLs where valid
			// assemblies could be loaded and used by the framework.

			UInt32 uiNumPlayers = 0;
			List<IPlayer> playerList = new List<IPlayer>();

			string   strCurDirectory = Directory.GetCurrentDirectory();
			string[] strFiles        = Directory.GetFiles(strCurDirectory, "*.dll", SearchOption.AllDirectories);

			Type simulationType = typeof(FrameworkInterface.Simulation.ISimulation);
			Type rendererType   = typeof(FrameworkInterface.Renderer.IRenderer);
			Type playerType     = typeof(FrameworkInterface.Player.IPlayer);

            // TEMP HACK NDP 2014
            bool bSkipDefaults = File.Exists(Path.Combine(strCurDirectory, "SkipDefault.txt"));

			foreach (string strCurDLL in strFiles)
			{
				string strFilename = Path.GetFileName(strCurDLL);

				try
				{
					Console.WriteLine("Trying to load " + strFilename);
					Assembly curLibrary = Assembly.LoadFile(strCurDLL);

					Type[] exportedTypes = curLibrary.GetExportedTypes();
					foreach (Type curType in exportedTypes)
					{
						// Ignore abstract classes
						if (curType.IsAbstract)
							continue;

						// Ignore interfaces
						if (curType.IsInterface)
							continue;

						// Is it an ISimulation?
						if (simulationType.IsAssignableFrom(curType))
						{
							if (mSimulation == null)
							{
								try
								{
									mSimulation = (FrameworkInterface.Simulation.ISimulation)Activator.CreateInstance(curType);
									Console.WriteLine("Using class " + curType.Name + " for the simulation.");
								}
								catch (System.Exception exCtor)
								{
									Console.WriteLine("Tried to use class " + curType.Name + " for the simulation, but following error occured: " + exCtor.Message);
								}
							}
						}
						
						// Is it an IRenderer?
						if (rendererType.IsAssignableFrom(curType))
						{
							if (mRenderer == null)
							{
								try
								{
									mRenderer = (FrameworkInterface.Renderer.IRenderer)Activator.CreateInstance(curType);
									Console.WriteLine("Using class " + curType.Name + " for the rendering.");
								}
								catch (System.Exception exCtor)
								{
									Console.WriteLine("Tried to use class " + curType.Name + " for the rendering, but following error occured: " + exCtor.Message);
								}
							}
						}
						
						// Is it an IPlayer?
						if (playerType.IsAssignableFrom(curType))
						{
                            if (bSkipDefaults)
                            {
                                // TEMP HACK NDP 2014
                                // Skip NullPlayer, RandomPlayer and SinusoidalPlayer
                                string szClassName = curType.Name;
                                if (szClassName.Equals("NullPlayer", StringComparison.InvariantCultureIgnoreCase))
                                    continue;
                                if (szClassName.Equals("RandomPlayer", StringComparison.InvariantCultureIgnoreCase))
                                    continue;
                                if (szClassName.Equals("SinusoidalPlayer", StringComparison.InvariantCultureIgnoreCase))
                                    continue;
                            }

							try
							{
								Console.WriteLine("Instanciating player " + curType.Name + " from " + strFilename);
								FrameworkInterface.Player.IPlayer newPlayer = (FrameworkInterface.Player.IPlayer)Activator.CreateInstance(curType);

								newPlayer.SetPlayerIndex(uiNumPlayers++);
								newPlayer.SetDLLPath(strCurDLL);

								playerList.Add(newPlayer);
							}
							catch (System.Exception exCtor)
							{
								Console.WriteLine("Tried to instantiate player class " + curType.Name + ", but following error occured: " + exCtor.Message);
							}
						}
					}
				}
				catch (System.Exception ex)
				{
					Console.WriteLine("Failed to load " + strFilename + " because " + ex.Message);
				}
			}

			// Validation
			if (mSimulation == null)
			{
				Console.WriteLine("No simulation could be found. Exiting.");
				return false;
			}

			if (mRenderer == null)
			{
				Console.WriteLine("No renderer could be found. Exiting.");
				return false;
			}

			if (playerList.Count == 0)
			{
				Console.WriteLine("No player could be found. Exiting.");
				return false;
			}

			mPlayers = playerList;
			return true;
		}

		static void StartGame()
		{
			// Start game simulation
			ICollection<IPlayer>      orderedPlayers = new List<IPlayer>(mPlayers);
			ICollection<IPlayerInput> startInputs    = mSimulation.OnGameStart_Begin(orderedPlayers);
			Debug.Assert(orderedPlayers.Count == startInputs.Count);

			// Notify players
			List<IPlayerOutput> playerOutputList = new List<IPlayerOutput>();
			var playerStartList = orderedPlayers.Zip(startInputs, (x, y) => new Tuple<IPlayer, IPlayerInput>(x, y));
			foreach (Tuple<IPlayer, IPlayerInput> curPlayer in playerStartList)
			{
				IPlayerInput playerInput = curPlayer.Item2;
				if (playerInput != null)
					playerInput = playerInput.CopyDeep();

				IPlayerOutput playerOutput = curPlayer.Item1.OnGameStart(playerInput);
				if (playerOutput != null)
					playerOutput = playerOutput.CopyDeep();

				playerOutputList.Add(playerOutput);
			}

			
			// Feed player output to simulation, and notify renderer
			FrameworkInterface.Simulation.ISimulationOutput simOutput = mSimulation.OnGameStart_End(playerOutputList);
			if (simOutput != null)
				simOutput = simOutput.CopyDeep();

			FrameworkInterface.Renderer.IRendererFeedback startFeedback = mRenderer.OnGameStart(simOutput);
			if (startFeedback != null)
				startFeedback = startFeedback.CopyDeep();

			mSimulation.ApplyRendererFeedback(startFeedback);
		}

		static void RunGame()
		{
			// Instantiate updater based on simulation type
			IUpdater updater = null;
			FrameworkInterface.Simulation.SimulationType_e eSimType = mSimulation.GetSimulationType();

			if (eSimType == FrameworkInterface.Simulation.SimulationType_e.kSimulationType_LockedStepSequential)
			{
				updater = new LockedStepSequentialUpdater();
			}
			else if (eSimType == FrameworkInterface.Simulation.SimulationType_e.kSimulationType_LockedStepParallel)
			{
				updater = new LockedStepParallelUpdater();
			}
			else if (eSimType == FrameworkInterface.Simulation.SimulationType_e.kSimulationType_RealTime)
			{
				updater = new RealTimeUpdater();
			}

			if (updater == null)
			{
				Console.WriteLine("Could not instantiate updater. Simulation return unsupported simulation type " + eSimType.ToString());
				return;
			}

			updater.Update(mSimulation, mRenderer, mPlayers);
		}

		static void EndGame()
		{
			// End game simulation
			ICollection<IPlayer> orderedPlayers = new List<IPlayer>(mPlayers);
			ICollection<IPlayerInput> endInputs = mSimulation.OnGameEnd_Begin(orderedPlayers);
			Debug.Assert(orderedPlayers.Count == endInputs.Count);

			// Notify players
			List<IPlayerOutput> playerOutputList = new List<IPlayerOutput>();
			var playerEndList = orderedPlayers.Zip(endInputs, (x, y) => new Tuple<IPlayer, IPlayerInput>(x, y));
			foreach (Tuple<IPlayer, IPlayerInput> curPlayer in playerEndList)
			{
				IPlayerInput playerInput = curPlayer.Item2;
				if (playerInput != null)
					playerInput = playerInput.CopyDeep();

				IPlayerOutput playerOutput = curPlayer.Item1.OnGameEnd(playerInput);
				if (playerOutput != null)
					playerOutput = playerOutput.CopyDeep();

				playerOutputList.Add(playerOutput);
			}

			// Feed player output to simulation, and notify renderer
			FrameworkInterface.Simulation.ISimulationOutput simOutput = mSimulation.OnGameEnd_End(playerOutputList);
			if (simOutput != null)
				simOutput = simOutput.CopyDeep();

			FrameworkInterface.Renderer.IRendererFeedback endFeedback = mRenderer.OnGameEnd(simOutput);
			if (endFeedback != null)
				endFeedback = endFeedback.CopyDeep();

			mSimulation.ApplyRendererFeedback(endFeedback);
		}
	}
}
